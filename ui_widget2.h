/********************************************************************************
** Form generated from reading UI file 'widget2.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET2_H
#define UI_WIDGET2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget2
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *label;

    void setupUi(QWidget *Widget2)
    {
        if (Widget2->objectName().isEmpty())
            Widget2->setObjectName(QString::fromUtf8("Widget2"));
        Widget2->resize(400, 300);
        horizontalLayout = new QHBoxLayout(Widget2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(Widget2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 170, 0);\n"
"font: 24pt \"Microsoft YaHei UI\";"));
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label);


        retranslateUi(Widget2);

        QMetaObject::connectSlotsByName(Widget2);
    } // setupUi

    void retranslateUi(QWidget *Widget2)
    {
        Widget2->setWindowTitle(QCoreApplication::translate("Widget2", "Form", nullptr));
        label->setText(QCoreApplication::translate("Widget2", "Widget2", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget2: public Ui_Widget2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET2_H
