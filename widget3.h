#ifndef WIDGET3_H
#define WIDGET3_H

#include <QWidget>

namespace Ui {
class Widget3;
}

class Widget3 : public QWidget
{
    Q_OBJECT

public:
    explicit Widget3(QWidget *parent = nullptr);
    ~Widget3();

private:
    Ui::Widget3 *ui;
};

#endif // WIDGET3_H
