/********************************************************************************
** Form generated from reading UI file 'widget3.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET3_H
#define UI_WIDGET3_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget3
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *label;

    void setupUi(QWidget *Widget3)
    {
        if (Widget3->objectName().isEmpty())
            Widget3->setObjectName(QString::fromUtf8("Widget3"));
        Widget3->resize(400, 300);
        horizontalLayout = new QHBoxLayout(Widget3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(Widget3);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 170, 0);\n"
"font: 24pt \"Microsoft YaHei UI\";"));
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label);


        retranslateUi(Widget3);

        QMetaObject::connectSlotsByName(Widget3);
    } // setupUi

    void retranslateUi(QWidget *Widget3)
    {
        Widget3->setWindowTitle(QCoreApplication::translate("Widget3", "Form", nullptr));
        label->setText(QCoreApplication::translate("Widget3", "Widget3", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget3: public Ui_Widget3 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET3_H
