#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QDir>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ;

    connect(ui->listWidget,&QListWidget::itemClicked,this,&MainWindow::on_itemClicked);

    m_widget1=new Widget1(this);
    m_widget2=new Widget2(this);
    m_widget3=new Widget3(this);


    ui->vLayout1->addWidget(m_widget1);
    ui->vLayout2->addWidget(m_widget2);
    ui->vLayout3->addWidget(m_widget3);

}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_itemClicked(QListWidgetItem* selectedItem)
{
    int index=ui->listWidget->currentRow();
    ui->stackedWidget->setCurrentIndex(index);
}

