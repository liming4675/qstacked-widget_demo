#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "widget1.h"
#include "widget2.h"
#include "widget3.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    Widget1*  m_widget1=nullptr;
    Widget2*  m_widget2=nullptr;
    Widget3*  m_widget3=nullptr;


    void on_itemClicked(QListWidgetItem* selectedItem);
};
#endif // MAINWINDOW_H
